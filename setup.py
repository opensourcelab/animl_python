"""_____________________________________________________________________

:PROJECT: AnIML_python

*animllib - a python implementation of AnIML.*

:details: animllib.

:authors: mark doerr (mark@uni-greifswald.de)
  
:date: (creation)    20200407

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

from typing import List
import os

from setuptools import setup, find_packages

package_name = 'animllib'

def read(filename):
    with open(os.path.join(os.path.dirname(__file__), filename), 'r') as file:
        return file.read()


def generate_package_data(input_path: str, ending: str = '') -> List[str]:
    paths = []
    for (path, _, files) in os.walk(input_path):
        paths.extend([os.path.join('..', path, file) for file in files if file.endswith(ending)])

    return paths

setup(
    name=package_name,
    version=__version__,
    description='animllib - a library for .',
    long_description=read('README_setup.rst'),
    author=', '.join([
        'Mark Doerr',
    ]),
    author_email='mark.doerr@uni-greifswald.de',
    keywords=('lab automation, laboratory, experiments, process management'),
    url='https://gitlab.com/animl/animl_python',
    license='MIT',
    packages=find_packages(),
    install_requires=[],
    test_suite='',
    classifiers=['License :: OSI Approved :: MIT License',
                 'Intended Audience :: Developers',
                 'Operating System :: OS Independent',
                 'Programming Language :: Python',
                 'Programming Language :: Python :: 3.7',
                 'Programming Language :: Python :: 3.8',
                 'Topic :: Utilities',
                 'Topic :: Scientific/Engineering',
                 'Topic :: Scientific/Engineering :: Interface Engine/Protocol Translator',
                 'Topic :: Scientific/Engineering :: Information Analysis'],
    include_package_data=True,
    package_data={ },
    setup_requires=['wheel', 'lxml']
)
