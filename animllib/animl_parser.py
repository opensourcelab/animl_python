"""_____________________________________________________________________

:PROJECT: AnIML_python

*animl_parser - parser for AnIML documents.*

:details:  parser for AnIML documents.

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)    20200408

.. todo:: - testing !!
________________________________________________________________________
"""

__version__ = "0.0.1"

import logging